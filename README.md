# Virginia Reels

Transcriptions in [abc notation](https://abcnotation.com/) of George P. Knauff's collection of Virginia Reels, published by George Willig of Baltimore, Maryland, in 1839.

I transcribed these tunes from scans found on public archive sites as a way of both learning to use abc notation and helping to practice reading music. There may be errors.

The one exception is "Midnight Serenade," for which I could not find a scan. The URL from which I got the abc file is in the file itself; I include it for sake of completeness.

Knauff's arrangements were for piano. I did not transcribe the bass cleff because I only wanted the melody. I may add the other voice at a later date.

The songs themselves and Knauff's sheet music are in the public domain, and I hereby place my transcriptions in the public domain as well.

Scans are available here:
    - [Number 1](https://levysheetmusic.mse.jhu.edu/collection/041/123)
    - Number 2 (I can't seem to find the link right now)
    - [Number 3](https://www.loc.gov/item/sm1839.012100/)
    - [Number 4](https://dc.lib.unc.edu/cdm/ref/collection/sheetmusic/id/9242)
